# Angular Blog Website

This repository contains the frontend code of the Angular Blog Website, a personal project that allows registered bloggers to write and share blogs on various topics. The website also enables bloggers to interact with blogs through comments and reactions.

## Functionalities

- User Registration using Firebase Authentication (using Email/Password )
- CRUD Operations:
  - Registered bloggers can add blogs to their list.
  - Admins can add blogs to the blog list.
  - Registered bloggers can edit/delete blogs.
- Security:
  - Implemented Authentication and Authorization using Firebase.
- Tools and Technologies:
  - Technology: HTML, MDBootstrap, CSS, Angular, Firebase.
  - Database: Angular Firebase.
- Covers all fundamentals of Angular, including:
  - Multiple Modules
  - Components, Template, and DataBinding
  - Form Validation
  - HttpClient
  - Animations
  - Dependency Injection
  - Routing & Navigation
  - Service Workers
  - Pipes
  - Guards, etc.

## Installation

Before running the project, ensure you have the following tools installed:

- Angular CLI. You can download it from [Angular CLI](https://angular.io/cli).
- NodeJs. You can download it from [Node.js](https://nodejs.org/).
- Package Manager: NPM or Yarn.

1. Clone this repository and navigate to the project directory.

2. Run the following command to install the project dependencies:

   ```bash
   npm install
   # or
   yarn install
